//
//  GameModelUnitTests.m
//  GameModelUnitTests
//
//  Created by Joseph Ong on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "GameModelUnitTests.h"
#import "Creature.h"
#import "Enemy.h"
#import "GameScene.h"

@implementation GameModelUnitTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

// test if all our constructors work properly!

- (void)testCreatureInitialization
{ 
    Creature *creature = [Creature getCreature];
    
    STAssertNotNil(creature.limbs, @"Creature limbs should not be nil.");
    STAssertNotNil(creature.joints, @"Creature joints should not be nil.");
    STAssertNotNil(creature.pulse, @"Creature pulse should not be nil.");
    
    
}

- (void)testElectricEelInitialization
{ 
    Enemy *enemy = [ElectricEel enemy];
    
    STAssertNotNil(enemy.limbs, @"EletricEel limbs should not be nil.");
    STAssertNotNil(enemy.joints, @"EletricEel joints should not be nil.");
    STAssertEquals((int) [enemy.limbs count], enemy.complexity, @"Complexity should match number of limbs.");
}

- (void)testStarfishInitialization
{ 
    Enemy *enemy = [Starfish enemy];
    
    STAssertNotNil(enemy.limbs, @"Starfish limbs should not be nil.");
    STAssertNotNil(enemy.joints, @"Starfish joints should not be nil.");
    STAssertEquals((int) [enemy.limbs count], enemy.complexity, @"Complexity should match number of limbs.");
}

- (void)testPlanktonInitialization
{ 
    Enemy *enemy = [Plankton enemy];
    
    STAssertNotNil(enemy.limbs, @"Plankton limbs should not be nil.");
    STAssertNotNil(enemy.joints, @"Plankton joints should not be nil.");
    STAssertEquals((int) [enemy.limbs count], enemy.complexity, @"Complexity should match number of limbs.");
}


@end
