//
//  MainMenuScene.m
//  Evo
//
//  Created by Joseph Ong on 4/24/12.
//  Copyright 2012 Harvard University. All rights reserved.
//

#import "MainMenuScene.h"

@implementation MainMenuScene

@synthesize start;

+(id) scene {
    CCScene *scene = [CCScene node];
    
    MainMenuScene *layer = [MainMenuScene node];
    [scene addChild: layer];
    
    layer.isTouchEnabled = YES;
    
    CCSprite *background = [[CCSprite alloc] initWithFile:@"MainMenuBackground.png"];
    [background setPosition:ccp(512, 384)];
    [layer addChild:background z:1];
    
    layer.start = [[CCSprite alloc] initWithFile:@"MainMenuStart.png"];
    [layer.start setPosition:ccp(512, 200)];
    [layer addChild:layer.start z:2];
    
    return scene;
}

- (id) init {
    if( (self=[super init]) ) 
    {
        // load the music
        SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
        if(audioEngine != nil) 
        {
            [audioEngine preloadBackgroundMusic:@"ambient.mp3"];
            if(audioEngine.willPlayBackgroundMusic) 
            {
                audioEngine.backgroundMusicVolume = 0.5f;
            }
        }
        
        // play the music
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"ambient.mp3"];
    }
    return self;
}

- (void) goToMapMenu {
    [[CCDirector sharedDirector] replaceScene: [GameScene scene]];
}


// handle touches
- (void)registerWithTouchDispatcher {
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event 
{
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    
    // if touching the start button, proceed to the game
    if(CGRectContainsPoint([start boundingBox], touchLocation)) {
        [self goToMapMenu];
    }
    
    return YES;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) dealloc {
    [super dealloc];
}
@end

