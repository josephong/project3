//
//  UpgradePanel.h
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameModel.h"
#import "Creature.h"
#import "CCTouchDispatcher.h"
#import "CCProgressTimer.h"

@interface UpgradePanel : CCLayer {
    
}

@property (nonatomic, assign) CCSprite *background;
@property (nonatomic, assign) CCLabelTTF *redCountLabel;
@property (nonatomic, assign) CCLabelTTF *greenCountLabel;
@property (nonatomic, assign) CCLabelTTF *blueCountLabel;

@property (nonatomic, assign) NSMutableDictionary *costLabels;
@property (nonatomic, assign) NSMutableDictionary *upgradeButtons;

@property (nonatomic, assign) CCSprite *pulseRadiusPurchaseBtn;
@property (nonatomic, assign) CCSprite *maxHealthPurchaseBtn;
@property (nonatomic, assign) CCSprite *movementSpeedPurchaseBtn;
@property (nonatomic, assign) CCSprite *pulseFrequencyPurchaseBtn;
@property (nonatomic, assign) CCSprite *healthRegenPurchaseBtn;
@property (nonatomic, assign) CCSprite *gemAttractionPurchaseBtn;
@property (nonatomic, assign) NSMutableDictionary *progressBars;


@property (nonatomic, assign) CCLabelTTF *pulseRadiusLabel;
@property (nonatomic, assign) CCLabelTTF *maxHealthLabel;
@property (nonatomic, assign) CCLabelTTF *movementSpeedLabel;



- (void)toggle;
- (void)updateResourceLabels;
- (void)updateProgressBars;
- (void)updateButtons;
@end
