//
//  GameScene.m
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene


- (id)init {
    if(self = [super init]) {
    }
    return self;
}

+ (id)scene {
    // initialize all our panels, adding them to the scene
    CCScene *scene = [CCScene node];
    
    GameLayer *gameLayer = [GameLayer node];
    [scene addChild:gameLayer z:1];
    
    ControlsLayer *controlsLayer = [ControlsLayer node];
    [scene addChild:controlsLayer z:2];
    
    UpgradePanel *upgradePanel = [UpgradePanel node];
    [upgradePanel setVisible:NO];
    [scene addChild:upgradePanel z:3];
    
    // save pointers to layers to model
    GameModel *gameModel = [GameModel getModel];
    [gameModel setGameLayer:gameLayer];
    
    [gameModel setUpgradePanel:upgradePanel];
    
    return scene;
}

@end
