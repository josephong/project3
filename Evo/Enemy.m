//
//  Enemy.m
//  Evo
//
//  Created by Joseph Ong on 4/24/12.
//  Copyright 2012 Harvard University. All rights reserved.
//

#import "Enemy.h"


@implementation Enemy

@synthesize limbs;
@synthesize joints;
@synthesize currentSpeed;
@synthesize maxSpeed;
@synthesize currentAngle;
@synthesize turnSpeed;
@synthesize complexity;
@synthesize randomDestination;
@synthesize HP;
@synthesize maxHP;
@synthesize HPRegen;
@synthesize pulseHitByCount;
@synthesize dying;
@synthesize overrideDestination;

#define ARC4RANDOM_MAX 0x100000000
#define HEALTH_COLOR_OFFSET 55
#define DEFAULT_DECELERATION_DISTANCE 200
#define DEFAULT_DECELERATION_FACTOR 6
#define DEFAULT_ACCELERATION_FACTOR 10
#define DESPERATION_THRESHOLD 50
#define JOINT_PIXEL_OFFSET 30

// updates the random destination for the enemy to path towards
- (void) updateRandomDestination:(ccTime)dt 
{
    GameModel *gameModel = [GameModel getModel];
 
    // finding bounds of rectangle to remain within map
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    int height = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.height;
    int width = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.width;
    
    CGFloat offsetX = -winSize.width/2;
    CGFloat offsetY = -winSize.height/2;
    
    // pick a random destination
    randomDestination = ccp(arc4random() % width + offsetX, arc4random() % height + offsetY);
}


- (void)colorByHP 
{
    // color a shade of red depending on the difference
    int difference = HEALTH_COLOR_OFFSET * HP/maxHP;
    for(Joint *limb in [self limbs])
        [limb setColor: ccc3(255 - difference, 200 + difference, 200)];
}

- (void) die 
{
    
    // if the sprite is already dying from a previous frame, don't fire this function again
    if(dying)
        return;
    
    // set dying to yes for previous condition to block
    dying = YES;
    
    GameModel *gameModel = [GameModel getModel];
    
    // splay joints out and generate gems when creature dies
    for (int i = 0; i < complexity; i++) 
    {
        // for each joint, splay it in a different direction and fade it out
        Joint *joint = [self.limbs objectAtIndex:i];
        id splay = [CCMoveBy actionWithDuration:.5 position:ccp(arc4random() % 150 + joint.position.x - 75, arc4random() % 150 + joint.position.y - 75)];
        id fade = [CCFadeOut actionWithDuration:.5];
        id splayAndFade = [CCSpawn actions:splay, fade, nil];
        
        // make sure to remove the joint from the parent sprite
        id cleanup = [CCCallBlock actionWithBlock:^(void){
            [self removeFromParentAndCleanup:YES];
        }];
        
        // run the sequence of actions
        [joint runAction:[CCSequence actions:splayAndFade, cleanup, nil]];
        
        // generate a random gem as a prize for the user
        NSString *gemType = nil;
        int r = arc4random() % 3;
        if(r == 0) gemType = @"blue";
        if(r == 1) gemType = @"red";
        if(r == 2) gemType = @"green";
        
        // place the gem at the old position of the joint, update the model accordingly
        Gem *gem = [Gem gemWithColor:gemType];
        gem.position = ccpAdd([self position], [joint position]);
        [[gameModel gameLayer] addChild:gem z:2];
        [[gameModel gems] addObject:gem];
    }
}

// default conform function is inverse kinematics. override for sepcific creature.
- (void)conformOverTime:(ccTime)dt
{   
    GameModel *gameModel = [GameModel getModel];
    
    // finding bounds of rectangle to remain within map
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    int height = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.height;
    int width = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.width;
    
    CGFloat offsetX = -winSize.width/2;
    CGFloat offsetY = -winSize.height/2;
    
    CGPoint destination;
    
    // run away from player if within detection radius, else wander randomly
    if(ccpDistance(self.position, [gameModel player].position) < 200 && !overrideDestination) 
    {
        // determine point that creature should run away to
        CGPoint delta = ccpSub([gameModel player].position, self.position);
        double slope = -atan2(delta.y, delta.x);
        double destX = self.position.x + 300 * cos(slope);
        double destY = self.position.y + 300 * sin(slope);
        
        // prevent creature from going into boundary region by heading toward the middle
        // as as desperate measure when cornered against the edge of the map
        if(destY - DESPERATION_THRESHOLD < offsetY || 
           destX - DESPERATION_THRESHOLD < offsetX ||
           destX + DESPERATION_THRESHOLD > width + offsetX || 
           destY + DESPERATION_THRESHOLD > height + offsetY) 
        {
            // need to set override flag to stop jittering at the wall
            destX = width/2 + offsetX;
            destY = height/2 + offsetY;
            overrideDestination = YES;
            
            // clear the override after four seconds
            id clearOverride = [CCCallBlock actionWithBlock:^(void) {
                overrideDestination = NO;
            }];
            
            [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:4.0f], clearOverride, nil]];
        }
        
        // needed to prevent jittering at boundary point
        destination = ccp(destX, destY);
        randomDestination = destination;
    }
    else 
        destination = randomDestination;
    
    // if you are approaching your destination, decelerate to a stop
    // else, you are far away from your destination, so accelerate up to the max speed
    if(ccpDistance(destination, self.position) < DEFAULT_DECELERATION_DISTANCE)
        currentSpeed = (currentSpeed < 0) ? 0 : currentSpeed - DEFAULT_DECELERATION_FACTOR;
    else
        currentSpeed = (currentSpeed > maxSpeed) ? maxSpeed : currentSpeed + DEFAULT_ACCELERATION_FACTOR;
    
    // get destination angle
    CGPoint diffInPosition = ccpSub(destination, self.position);
    double destAngle = atan2(diffInPosition.y, diffInPosition.x);
    
    // make sure the angle is bounded between 0 and 2pi
    if(destAngle < 0) destAngle += 2 * M_PI;
    if(destAngle > 2 * M_PI) destAngle -= 2 * M_PI;
    
    // make sure our current angle is bounded between 0 and 2pi
    if(currentAngle > 2 * M_PI) currentAngle -= 2 * M_PI;
    if(currentAngle < 0) currentAngle += 2 * M_PI;
    
    // compare the angles
    if(currentAngle < destAngle) 
    {
        // if within a certain threshold, just rotate completely
        if((destAngle - currentAngle) <= turnSpeed * dt) 
            currentAngle = destAngle;
        else 
        {
            // else, turn slowly to face that direction
            if(destAngle - currentAngle >= M_PI)
                currentAngle -= turnSpeed * dt;
            else
                currentAngle += turnSpeed * dt;
        }
    }
    
    // if the opposite holds true, rotate the other direction
    if(currentAngle >= destAngle) 
    {
        if((-destAngle + currentAngle) <= turnSpeed * dt)
            currentAngle = destAngle;
        else 
        {
            if(currentAngle - destAngle >= M_PI)
                currentAngle += turnSpeed * dt;
            else
                currentAngle -= turnSpeed * dt;
        }
        
    }
    
    // make sure the angle is still bounded between 0 and 2pi after alteration
    if(currentAngle > 2 * M_PI) currentAngle -= 2 * M_PI;
    if(currentAngle < 0) currentAngle += 2 * M_PI;
    
    // update the rotation of the head
    [[self.limbs objectAtIndex:0] updateAngle:currentAngle];
    
    // calculate the delta distance from the current point
    double diffX = currentSpeed * cos(currentAngle) * dt;
    double diffY = currentSpeed * sin(currentAngle) * dt;
    CGPoint constrainedDiffInPosition = ccp(diffX, diffY);
    
    // change our position by that delta
    self.position = ccpAdd(self.position, constrainedDiffInPosition);
    
    // shift all subsequent joints so they rotate and move in the direction of the prior joint
    for (int i = 1; i < complexity; i++) 
    {
        Joint *joint = [self.limbs objectAtIndex:i];
        Joint *previousJoint = [self.limbs objectAtIndex:i-1];
        
        [joint updateAngle:ccpToAngle(ccpSub([previousJoint position], [joint secondEndpoint]))];
        joint.position = ccpAdd([previousJoint secondEndpoint], ccpSub(joint.position, [joint firstEndpoint]));
    }
    [self colorByHP];
}

@end

@implementation ElectricEel

#define EEL_BASE_NUM_JOINTS 4
#define EEL_JOINT_NUM_RANGE 4

// create an eel
+ (Enemy *) enemy 
{
    ElectricEel *eel = [ElectricEel node];
    
    for(int i = 0; i < arc4random() % EEL_BASE_NUM_JOINTS + EEL_JOINT_NUM_RANGE; i++) 
    {
        // first joint is an arrow, all others are the dna sprites
        Joint *joint = (i == 0) ? (Joint*) [Joint jointWithFile:@"Eel1-small.png"] :
            (Joint*) [Joint jointWithFile:@"Eel2-small.png"];

        [joint setEndpoints:ccp(0, i * JOINT_PIXEL_OFFSET) :ccp(0, JOINT_PIXEL_OFFSET + i * JOINT_PIXEL_OFFSET)];
        CGPoint jointPosition = ccp(0, i * JOINT_PIXEL_OFFSET);
        
        // update the model to keep track of the joints
        [[eel joints] addObject:NSStringFromCGPoint(jointPosition)];
        [[eel limbs] addObject:joint];
        [eel addChild:joint];
        
        // increase the eel's complexity
        eel.complexity++;
    }
    return eel;
}


- (id)init 
{
    // base stats of the eel depends on the difficulty of current level
    GameModel *gameModel = [GameModel getModel];
    int difficulty = MAX((int) [gameModel level].x, (int) [gameModel level].y);
    
    if(self = [super init]) 
    {
        joints = [[NSMutableArray alloc] init];
        limbs = [[NSMutableArray alloc] init];
        complexity = 0;
        turnSpeed = 10;
        currentSpeed = 200;
        maxSpeed = 270 + difficulty * 10;
        HP = 50 + difficulty * 5;
        maxHP = HP;
        pulseHitByCount = -1;
        overrideDestination = NO;
        
        // set to find a random destination at an interval between 3 and 4 seconds
        [self schedule:@selector(updateRandomDestination:) interval:((double) arc4random()/ARC4RANDOM_MAX * 1.0f + 2.0f)];
    }
    return self;
}

@end

@implementation Starfish

// create a starfish
+ (Enemy *) enemy 
{
    // similar as others, just add the sprite and joint
    Starfish *starfish = [Starfish node];
    Joint *joint = (Joint *) [Joint jointWithFile:@"starfish-small.png"];
    
    [joint setEndpoints:ccp(0, 0) :ccp(0, JOINT_PIXEL_OFFSET)];
    [[starfish joints] addObject:NSStringFromCGPoint(ccp(0, 0))];
    [[starfish limbs] addObject:joint];
    [starfish addChild:joint];
    starfish.complexity = 1;
    
    return starfish;
}

- (id)init 
{
    if(self = [super init]) 
    {
        // base stats of the starfish depends on the difficulty of current level
        GameModel *gameModel = [GameModel getModel];
        int difficulty = MAX((int) [gameModel level].x, (int) [gameModel level].y);
        
        joints = [[NSMutableArray alloc] init];
        limbs = [[NSMutableArray alloc] init];
        complexity = 0;
        turnSpeed = 10;
        currentSpeed = 100;
        maxSpeed = 200;
        HP = 20 + difficulty * 3;
        maxHP = 20 + difficulty * 3;
        pulseHitByCount = -1;
        overrideDestination = NO;
        
        // set to find a random destination at an interval between 2 and 3 seconds
        [self schedule:@selector(updateRandomDestination:) interval:((double) arc4random()/ARC4RANDOM_MAX * 1.0f + 2.0f)];
    }
    return self;
}

@end

// UNUSED FOR NOW. TODO: Fix Plankton joint mechanics, it needs a floaty conform function
@implementation Plankton

// create a starfish
+ (Enemy *) enemy {
    Plankton *plankton = [Plankton node];
    
    Joint *joint = (Joint *) [Joint jointWithFile:@"plankton-body-small.png"];
    
    [joint setEndpoints:ccp(0, 0) :ccp(0, 30)];
    [[plankton joints] addObject:NSStringFromCGPoint(ccp(0, 0))];
    [[plankton limbs] addObject:joint];
    [plankton addChild:joint];
    
    Joint *leg1 = (Joint *) [Joint jointWithFile:@"plankton-leg-small.png"];
    [leg1 setEndpoints:ccp(13, 15) :ccp(13, 15)];
    [[plankton joints] addObject:NSStringFromCGPoint(ccp(13, 15))];
    [[plankton limbs] addObject:leg1];
    [plankton addChild:leg1];
    
    Joint *leg2 = (Joint *) [Joint jointWithFile:@"plankton-leg-small.png"];
    [leg1 setEndpoints:ccp(14, 15) :ccp(14, 15)];
    [[plankton joints] addObject:NSStringFromCGPoint(ccp(14, 15))];
    [[plankton limbs] addObject:leg2];
    [plankton addChild:leg2];
    
    Joint *leg3 = (Joint *) [Joint jointWithFile:@"plankton-leg-small.png"];
    [leg1 setEndpoints:ccp(15, 15) :ccp(15, 15)];
    [[plankton joints] addObject:NSStringFromCGPoint(ccp(15, 15))];
    [[plankton limbs] addObject:leg3];
    [plankton addChild:leg3];
    
    plankton.complexity = 4;
    
    return plankton;
}

- (id)init {
    if(self = [super init]) {
        GameModel *gameModel = [GameModel getModel];
        int difficulty = MAX((int) [gameModel level].x, (int) [gameModel level].y);
        
        joints = [[NSMutableArray alloc] init];
        limbs = [[NSMutableArray alloc] init];
        complexity = 0;
        turnSpeed = 10;
        currentSpeed = 100;
        maxSpeed = 200;
        HP = 20 + difficulty * 3;
        maxHP = 20 + difficulty * 3;
        pulseHitByCount = -1;
        overrideDestination = NO;
        
        [self schedule:@selector(updateRandomDestination:) interval:((double) arc4random()/ARC4RANDOM_MAX * 1.0f + 2.0f)];
    }
    return self;
}


- (void)conformOverTime:(ccTime)dt{
    GameModel *gameModel = [GameModel getModel];
    
    // finding bounds of rectangle to remain within map
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    int height = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.height;
    int width = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.width;
    
    CGFloat offsetX = -winSize.width/2;
    CGFloat offsetY = -winSize.height/2;
    
    // run away from player if within detection radius, else wander randomly
    CGPoint destination;
    
    if(ccpDistance(self.position, [gameModel player].position) < 200 && !overrideDestination) {
        // determine point that creature should run away to
        CGPoint delta = ccpSub([gameModel player].position, self.position);
        double slope = -atan2(delta.y, delta.x);
        double destX = self.position.x + 300 * cos(slope);
        double destY = self.position.y + 300 * sin(slope);
        
        // prevent creature from going into boundary region by heading toward the middle
        // as as desperate measure when cornered
        if(destY - 50 < offsetY || destX - 50 < offsetX ||
           destX + 50 > width + offsetX || destY + 50 > height + offsetY) 
        {
            destX = width/2 + offsetX;
            destY = height/2 + offsetY;
            overrideDestination = YES;
            
            // need to set override flag to stop "running away" against a wall
            id clearOverride = [CCCallBlock actionWithBlock:^(void){
                overrideDestination = NO;
            }];
            
            [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:4.0f], clearOverride, nil]];
        }
        
        destination = ccp(destX, destY);
        
        // set random destination to destination, if not creature will jitter at boundary
        // its detection radius
        randomDestination = destination;
    }
    else {
        destination = randomDestination;
    }
    
    // if you are approaching your destination, decelerate to a stop
    // else, you are far away from your destination, so accelerate up to the max speed
    if(ccpDistance(destination, self.position) < 100)
        currentSpeed = (currentSpeed < 0) ? 0 : self.currentSpeed - 6;
    else
        currentSpeed = (currentSpeed > maxSpeed) ? maxSpeed : currentSpeed + 10;
    
    // get destination angle
    CGPoint diffInPosition = ccpSub(destination, self.position);
    double destAngle = atan2(diffInPosition.y, diffInPosition.x);
    
    if(destAngle < 0) destAngle += 2*M_PI;
    
    currentAngle = destAngle;
    
    [[self.limbs objectAtIndex:0] updateAngle:currentAngle];
    
    double diffX = currentSpeed * cos(currentAngle) * dt;
    double diffY = currentSpeed * sin(currentAngle) * dt;
    
    CGPoint constrainedDiffInPosition = ccp(diffX, diffY);
    self.position = ccpAdd(self.position, constrainedDiffInPosition);
    
    for (int i = 1; i < complexity; i++) {
        Joint *joint = [self.limbs objectAtIndex:i];
        
        [joint updateAngle:currentAngle];
        joint.position = ccpAdd([joint firstEndpoint], ccpSub(joint.position, [joint secondEndpoint]));
    }
    
    [self colorByHP];
}

@end


@implementation Tadpole

// create an tadpole!
+ (Enemy *) enemy 
{
    Tadpole *tadpole = [Tadpole node];
    
    for(int i = 0; i < 3; i++) {
        Joint *joint;
        
        switch (i) {
            case 0:
                joint = [Joint jointWithFile:@"tadpole-head-small.png"];
                break;
                
            case 1:
                joint = [Joint jointWithFile:@"tadpole-body-small.png"];
                break;
                
            case 2:
                joint = [Joint jointWithFile:@"tadpole-tail-small.png"];
                break;
                
            default:
                joint = [Joint jointWithFile:@"tadpole-body-small.png"];
                break;
        }
        
        [joint setEndpoints:ccp(0, i * JOINT_PIXEL_OFFSET) :ccp(0, JOINT_PIXEL_OFFSET + i * JOINT_PIXEL_OFFSET)];
        CGPoint jointPosition = ccp(0, i * JOINT_PIXEL_OFFSET);
        [[tadpole joints] addObject:NSStringFromCGPoint(jointPosition)];
        [[tadpole limbs] addObject:joint];
        [tadpole addChild:joint];
        tadpole.complexity = tadpole.complexity + 1;
    }
    return tadpole;
}


- (id)init 
{
    // again, stats are based on where the user is located in the grid
    GameModel *gameModel = [GameModel getModel];
    int difficulty = MAX((int) [gameModel level].x, (int) [gameModel level].y);
    
    if(self = [super init]) 
    {
        joints = [[NSMutableArray alloc] init];
        limbs = [[NSMutableArray alloc] init];
        complexity = 0;
        turnSpeed = 10;
        currentSpeed = 200;
        maxSpeed = 150 + difficulty * 10;
        HP = 50 + difficulty * 5;
        maxHP = HP;
        pulseHitByCount = -1;
        overrideDestination = NO;
        
        // set to find a random destination at an interval between 3 and 4 seconds
        [self schedule:@selector(updateRandomDestination:) interval:((double) arc4random()/ARC4RANDOM_MAX * 1.0f + 3.0f)];
    }
    return self;
}

@end
