//
//  GameLayer.m
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLayer.h"


@implementation GameLayer

@synthesize movementTouchLocation;
@synthesize movementTouchLocationGL;
@synthesize movementTouched;
@synthesize map;
@synthesize levelLabel;
@synthesize levelInstructions;
@synthesize killInstructions;

#define LEFT -1
#define RIGHT 1
#define BOTTOM -2
#define TOP 2
#define CENTER 0
#define BASE_ATTRACION 100
#define ATTRACTION_UPGRADE_FACTOR 10
#define KILL_UPGRADE_TIER 7
#define MAX_UPGRADE_TIER 35

- (id)init 
{
    if (self = [super init]) 
   
{
        GameModel *gameModel = [GameModel getModel];
        
        // add map sprite to screen
        map = [[CCSprite alloc] initWithFile:@"map.png"];
        [map setBlendFunc:(ccBlendFunc){GL_ONE, GL_ZERO}];
        [map setPosition:ccp(512, 384)];
        [self addChild:map z:0];
        [[gameModel mapLayers] addObject:map];
        
        // create the player
        Creature *player = [Creature getCreature];
        [self addChild:player z:4];
        [player setPosition:ccp(512,384)];
        [gameModel setPlayer:player];
        
        // generate a iniial random patch of enemies
        [self generateRandomEnemies];
        
        // set the game loops to run once per frame
        [self schedule:@selector(creatureLogic:)];
        [self schedule:@selector(gemLogic:)];
        [self schedule:@selector(damageLogic:)];
        
        [gameModel setPlaying:YES];
        
        // set resources to 0
        [[gameModel resources] removeAllObjects];
        [[gameModel resources] setObject:[NSNumber numberWithInt:0] forKey:@"blue"];
        [[gameModel resources] setObject:[NSNumber numberWithInt:0] forKey:@"red"];
        [[gameModel resources] setObject:[NSNumber numberWithInt:0] forKey:@"green"];
        
        // default string for the level label in the middle of the screen
        levelLabel = [[CCLabelTTF alloc] initWithString:@"0, 0" fontName:@"League Gothic.otf" fontSize:144];
        [levelLabel setOpacity:50];
        [levelLabel setScale:4];
        [self addChild:levelLabel z:1];
    
        // default string for the level label in the middle of the screen
        levelInstructions = [[CCLabelTTF alloc] initWithString:@"Visit an edge of the map to travel to a new realm." fontName:@"League Gothic.otf" fontSize:36];
        [levelInstructions setOpacity:50];
        [levelInstructions setScale:1.5];
        [self addChild:levelInstructions z:1];
        
        // default string for the level label in the middle of the screen
        killInstructions = [[CCLabelTTF alloc] initWithString:@"Consume other organisms to evolve." fontName:@"League Gothic.otf" fontSize:36];
        [killInstructions setOpacity:50];
        [killInstructions setPosition:ccp(512, 384)];
        [self addChild:killInstructions z:1];
    
        self.isTouchEnabled = YES;
    }
    return self;
}

- (void) createNewLevel 
{
    // choose a random map color, destroy the current enemies and generate new ones
    [map setColor: ccc3(arc4random() % 256, arc4random() % 256, arc4random() % 256)];
    [self destroyAllEnemies];
    [self generateRandomEnemies];
    
    // update the model to reflect that we're in a new level
    GameModel *gameModel = [GameModel getModel];
    CGPoint currentLevel = [gameModel level];
    [[self levelLabel] setString:[NSString stringWithFormat:@"%d, %d",(int) currentLevel.x, (int) currentLevel.y]];
    
    // display the kill and level instructions only when on (0,0)
    if(currentLevel.x == 0 && currentLevel.y == 0) 
    {
        levelInstructions.visible = YES;
        killInstructions.visible = YES;
    }
    else 
    {
        levelInstructions.visible = NO;
        killInstructions.visible = NO;
    }
}

// returns a valid random location on the map
- (CGPoint) generateRandomDestination 
{
    GameModel *gameModel = [GameModel getModel];
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    // find the coordinate bounds of our map, which go from the coordinate bounds
    // (x, y) in ([offsetX, width - offsetX], [offsetY, height - offsetY])
    int height = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.height;
    int width = (int) [[[gameModel mapLayers] objectAtIndex:0] boundingBox].size.width;
    CGFloat offsetX = -winSize.width/2;
    CGFloat offsetY = -winSize.height/2;
    
    return ccp(arc4random() % width + offsetX, arc4random() % height + offsetY);
}

- (void)generateRandomEnemies 
{
    GameModel *gameModel = [GameModel getModel];
    
    // types of creatures. will do plist eventually. commenting out plankton for now, which
    // looks terrible boo.
    NSMutableArray *types = [[NSMutableArray alloc] initWithObjects:@"Starfish", @"ElectricEel", @"Tadpole"/*, @"Plankton"*/, nil];
    
    // generate between 6 and 14 enemies
    int numberOfEnemies = arc4random() % 8 + 6;
    for(int i = 0; i < numberOfEnemies; i++)
    {
        // pick a random enemy type, and place at a random location on the map
        Class arrayClass = NSClassFromString([types objectAtIndex:(arc4random() % [types count])]);
        id enemy = [arrayClass enemy];
        [self addChild:enemy];
        [enemy setPosition:[self generateRandomDestination]];
        
        // add it to the model so we can track it
        [[gameModel enemies] addObject:enemy];
    }
}

- (void)destroyAllEnemies 
{
    GameModel *gameModel = [GameModel getModel];
    
    // remove all enemy sprites from the game layer
    for(Enemy* enemy in [gameModel enemies])
        [enemy removeFromParentAndCleanup:YES];
    
    // flush the model
    [[gameModel enemies] removeAllObjects];
}

- (void)setViewpointCenter:(CGPoint) center
{
    // first, get the size of the window
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    // prevent the map center from exceeding the map edges on the low end
    NSInteger x = MAX(center.x, 0);
    NSInteger y = MAX(center.y, 0);
    
    // prevent the map center from exceeding the map edges on the high end
    x = MIN(x, [map boundingBox].size.width - winSize.width);
    y = MIN(y, [map boundingBox].size.height - winSize.height);
    
    // shift the coordinates to the middle of the viewport
    x = winSize.width/2 - x;
    y = winSize.height/2 - y;
    
    CGPoint position = ccp(x, y);
    self.position = position;
    
    // center map label as well
    [self levelLabel].position = position;
}

- (void)registerWithTouchDispatcher
{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}
         
- (void)creatureLogic:(ccTime)dt
{
    GameModel *gameModel = [GameModel getModel];
    if(![gameModel playing]) return;
    CGSize winSize = [[CCDirector sharedDirector] winSize];
   
    CGPoint currentLevel = [gameModel level];
    
    // check if the creature is on the edge of the map. destroy all enemies, transition to a new level after.
    switch ([[gameModel player] isAtEdge])
    {
        case LEFT:
            [gameModel setLevel:ccp(currentLevel.x - 1, currentLevel.y)];
            [[gameModel player] setPosition:ccp(winSize.width/2, winSize.height/2)];
            [self createNewLevel];
            break;
        
        case RIGHT:
            [gameModel setLevel:ccp(currentLevel.x + 1, currentLevel.y)];
            [[gameModel player] setPosition:ccp(winSize.width/2, winSize.height/2)];
            [self createNewLevel];
            break;
        
        case BOTTOM:
            [gameModel setLevel:ccp(currentLevel.x, currentLevel.y - 1)];
            [[gameModel player] setPosition:ccp(winSize.width/2, winSize.height/2)];
            [self createNewLevel];
            break;
        
        case TOP:
            [gameModel setLevel:ccp(currentLevel.x, currentLevel.y + 1)];
            [[gameModel player] setPosition:ccp(winSize.width/2, winSize.height/2)];
            [self createNewLevel];
            break;
            
        default:
            // else conform to new touch location and pulse
            [[gameModel player] conform:movementTouchLocation overTime:dt];
            [[gameModel player] pulseOverTime:dt];
            break;
    };
    
    // make the enemies respond to our creature
    for(Enemy* enemy in [gameModel enemies])
        [enemy conformOverTime:dt];
    
    [self setViewpointCenter:[gameModel player].position];
}

- (void)gemLogic:(ccTime)dt
{
    GameModel *gameModel = [GameModel getModel];
    if (![gameModel playing]) return;
    
    NSMutableArray *gemsToDelete = [[NSMutableArray alloc] init];
    for (Gem *gem in [gameModel gems])
    {
        for (Joint *joint in [[gameModel player] limbs])
        {
            // calculate position of joint relative to map and check if it hits a gem
            CGPoint jointPosition = ccpAdd([joint position], [[gameModel player] position]);
            if(ccpDistance(jointPosition, [gem position]) < 20)
                [gemsToDelete addObject:gem];
        }
    }
    
    // delete gem and add to resources if we detected a collision
    for (Gem *gem in gemsToDelete)
    {
        [[gameModel gems] removeObject:gem];
        [self removeChild:gem cleanup:YES];
        
        // increment resource count in game model
        int resourceCount = [(NSNumber *) [[gameModel resources] objectForKey:[gem type]] intValue];
        [[gameModel resources] setObject:[NSNumber numberWithInt:(resourceCount + 1)] forKey:[gem type]];
    }
    
    // attract the gem to player if the gem is in the attraction radius
    for (Gem *gem in [gameModel gems])
    {
        for (Joint *joint in [[gameModel player] limbs]) 
        {
            // calculate position of joint relative to map and check if it hits a gem
            CGPoint jointPosition = ccpAdd([joint position], [[gameModel player] position]);
            
            // calculate gem attraction radius of the player
            int gemAttractionDistance = BASE_ATTRACION + ATTRACTION_UPGRADE_FACTOR * [(NSNumber *)[[gameModel upgradeLevels] objectForKey:@"movementSpeed"] intValue];
            
            // if the gem is within the attraction radius, move it by a fex pixels in that direction
            if(ccpDistance(jointPosition, [gem position]) < gemAttractionDistance) 
            {
                CGPoint diffVector = ccpNormalize(ccpSub(jointPosition, [gem position]));
                gem.position = ccpAdd([gem position], ccpMult(diffVector, 2.5));
                break;
            }
        }
    }
}


// check for pulse interactions between player and enemies, and update HP
- (void)damageLogic:(ccTime)dt {
    GameModel *gameModel = [GameModel getModel];
    
    // regenerates user HP every dt
    [[gameModel player] regen];
    
    // find the pulse center
    CGPoint pulseCenter = ccpAdd([[gameModel player] position], [[[gameModel player] pulse] position]);
    
    NSMutableArray *enemiesToRemove = [[NSMutableArray alloc] init];
    
    // for each enemy in the model
    for (Enemy *enemy in [gameModel enemies]) 
    {
        // if we hit this enemy already with this pulse in a previous frame, skip it
        if ([[gameModel player] pulseCount] == [enemy pulseHitByCount]) continue;
        
        for (Joint *limb in [enemy limbs]) 
        {
            CGPoint limbPosition = ccpAdd([enemy position], [limb position]);
            double distanceToPulseCenter = ccpDistance(limbPosition, pulseCenter);
            
            // if any limb of that enemy comes in contact with the pulse
            if (distanceToPulseCenter < [[gameModel player] currentPulseRadius]) 
            {
                // damage the enemy
                [enemy setPulseHitByCount:[[gameModel player] pulseCount]];
                enemy.HP -= [[gameModel player] damage];
                
                // if the enemy dies add it to the list to be removed
                if(enemy.HP <= 0) 
                    [enemiesToRemove addObject:enemy];
                
                // don't count multiple hits to joints
                break;
            }
        }
    }
    
    // if the enemy died, remove it form the model
    for (Enemy *enemy in enemiesToRemove) 
    {
        [[gameModel enemies] removeObject:enemy];
        [enemy die];
        
        // increment kill count
        gameModel.player.killCount++;
        
        // every KILL_UPGRADE_TIER kills, add a joint, for a max of
        // MAX_UPGRADE_TIER/KILL_UPGRADE_TIER additional joints
        if([gameModel player].killCount % KILL_UPGRADE_TIER == 0 &&
           [gameModel player].killCount < MAX_UPGRADE_TIER)
        {
            [[gameModel player] addJoint];
        }
    }
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    // determine where the user touched, so the player can conform to it later
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    movementTouchLocationGL = touchLocation;
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    movementTouchLocation = touchLocation;
    movementTouched = YES;
    
    [self schedule:@selector(updateTouchLocations)];
    
    return YES;
}

- (void)updateTouchLocations 
{
    // convert the touch location to the proper reference frame at each frame
    movementTouchLocation = [self convertToNodeSpace:movementTouchLocationGL];
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event 
{
    // update the user touch location if the finger is dragged
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    movementTouchLocationGL = touchLocation;
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    movementTouchLocation = touchLocation;
    movementTouched = YES;
   
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event 
{
    // the snake stops at the location where you let go.
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    movementTouchLocation = touchLocation;
    movementTouched = NO;
    
    [self unschedule:@selector(updateTouchLocations)];
}
@end
