//
//  Joint.m
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Joint.h"


@implementation Joint

@synthesize firstEndpoint;
@synthesize secondEndpoint;
@synthesize angle;

#define JOINT_PIXEL_OFFSET 30

// return a joint object
+ (Joint *)jointWithFile:(NSString *)file
{
    // allocates a joint, whose sprite comes from the file we feed it
    Joint *joint = (Joint*) [[Joint alloc] initWithFile:file];
    joint.angle = 0;
    
    return joint;
}

- (void)setEndpoints:(CGPoint)point1 :(CGPoint)point2 
{    
    // find the rotation given the new endpoints of the sprite
    angle = ccpToAngle(ccpSub(point2, point1));
    
    // find the new middle, based on the midpoint
    CGPoint newPosition = ccpMidpoint(point1, point2);
    [self setPosition:newPosition];
    
    // set the rotation of the sprite
    [self setRotation:CC_RADIANS_TO_DEGREES(-1 * angle)];
}

- (void)updateAngle:(double)a 
{
    // function to manually set angle of a sprite
    self.angle = a;
    self.rotation = CC_RADIANS_TO_DEGREES(-1 * self.angle);
}

- (CGPoint)firstEndpoint 
{
    // calculates and returns the first endpoint 
    double diffX = cos(angle) * self.scaleX/2 * JOINT_PIXEL_OFFSET;
    double diffY = sin(angle) * self.scaleX/2 * JOINT_PIXEL_OFFSET;
    
    return ccpAdd(self.position, ccp(diffX, diffY));
}

- (CGPoint)secondEndpoint 
{
    // calculates and returns the second endpoint
    double diffX = cos(angle) * self.scaleX/2 * JOINT_PIXEL_OFFSET;
    double diffY = sin(angle) * self.scaleX/2 * JOINT_PIXEL_OFFSET;
    
    return ccpSub(self.position, ccp(diffX, diffY));
}


- (id)init {
    if (self = [super init]) {

    }
    return self;
}

@end
