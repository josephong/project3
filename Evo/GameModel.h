//
//  GameModel.h
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class GameLayer;
@class ControlsLayer;
@class Creature;
@class UpgradePanel;

@interface GameModel : NSObject

@property (nonatomic, assign) CCScene *gameScene;

// pointers to UI layers
@property (nonatomic, assign) GameLayer *gameLayer;
@property (nonatomic, assign) ControlsLayer *controlsLayer;
@property (nonatomic, assign) UpgradePanel *upgradePanel;

// game sprites
@property (nonatomic, assign) Creature *player;
@property (nonatomic, assign) NSMutableArray *enemies;
@property (nonatomic, assign) NSMutableArray *mapLayers;
//@property (nonatomic, assign) NSMutableArray *projectiles;
@property (nonatomic, assign) NSMutableArray *gems;

// upgrade data
@property (nonatomic, assign) NSMutableArray *upgrades;
@property (nonatomic, assign) NSMutableDictionary *upgradeCosts;
@property (nonatomic, assign) NSMutableDictionary *upgradeLevels;
@property (nonatomic, assign) NSMutableDictionary *upgradeTypes;

// ordered pair representing level in x and y direction
@property (nonatomic, assign) CGPoint level;

@property (nonatomic, assign) BOOL playing;

// player variables
//@property (nonatomic, assign) int xpCurrent;
//@property (nonatomic, assign) int level;
@property (nonatomic, assign) NSMutableDictionary *resources;


// get the unique saved game model
+ (GameModel*)getModel;
@end
