//
//  ControlsLayer.h
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTouchDispatcher.h"
#import "SimpleAudioEngine.h"
#import "GameModel.h"
#import "UpgradePanel.h"

@interface ControlsLayer : CCLayer {
    
}

@property (nonatomic, assign) CCSprite *upgradesButton;
@property (nonatomic, assign) CCSprite *upgradesLabel;
@property (nonatomic, assign) CCSprite *muteButton;

@end
