//
//  Joint.h
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Joint : CCSprite {
    
}

@property (nonatomic, assign) CGPoint firstEndpoint;
@property (nonatomic, assign) CGPoint secondEndpoint;
@property (nonatomic, assign) double angle;
+ (Joint *)jointWithFile:(NSString *)file;
- (void)setEndpoints:(CGPoint)point1 :(CGPoint)point2;
- (void)updateAngle:(double)a;
@end
