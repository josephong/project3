//
//  ControlsLayer.m
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ControlsLayer.h"


@implementation ControlsLayer

@synthesize upgradesButton;
@synthesize upgradesLabel;
@synthesize muteButton;

- (id)init {
    if (self = [super init]) 
    {
        // add a mute button in the bottom left corner
        muteButton = [[CCSprite alloc] initWithFile:@"sound-on.png"];
        [muteButton setPosition:ccp(34, 30)];
        [self addChild:muteButton z:1];
        
        // add upgrade plus button in right corner
        upgradesButton = [[CCSprite alloc] initWithFile:@"plus.png"];
        [upgradesButton setPosition:ccp(990, 30)];
        [self addChild:upgradesButton z:1];
        
        // add upgrade label to the button
        upgradesLabel = [[CCLabelTTF alloc] initWithString:@"Upgrades" fontName:@"League Gothic.otf" fontSize:144];
        [upgradesLabel setOpacity:90];
        [upgradesLabel setScale:.25];
        [upgradesLabel setPosition:ccp(920, 30)];
        [self addChild:upgradesLabel z:1];
        
        self.isTouchEnabled = YES;
    }
    return self;
}

- (void)registerWithTouchDispatcher 
{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}



- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event 
{
    GameModel *gameModel = [GameModel getModel];
    
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    // if we touch the plus, open the panel!
    if (CGRectContainsPoint([upgradesButton boundingBox], touchLocation)) 
    {
        [[gameModel upgradePanel] toggle];
        
        // remove the upgrades text label after the first time tapping to make more simplistic
        if(upgradesLabel.visible)
        {
            id fade = [CCFadeOut actionWithDuration:.5];
            id toggleVisibility = [CCCallBlock actionWithBlock:^(void){
                upgradesLabel.visible = NO;
            }];
            [upgradesLabel runAction:[CCSequence actions:fade, toggleVisibility, nil]];
        }
        
        return YES;
    }
    
    if (CGRectContainsPoint([muteButton boundingBox], touchLocation))
    {
        SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
        if([audioEngine isBackgroundMusicPlaying])
        {
            [muteButton setTexture:[[CCTextureCache sharedTextureCache] addImage:@"sound-off.png"]];
            [audioEngine pauseBackgroundMusic];
        }
        else 
        {
            [muteButton setTexture:[[CCTextureCache sharedTextureCache] addImage:@"sound-on.png"]];
            [audioEngine resumeBackgroundMusic];
        }
        
        return YES;
    }
    
    return NO;
}

@end
