//
//  Gem.h
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameModel.h"

@interface Gem : CCSprite {
    
}

@property (nonatomic, assign) NSString *type;

+ (Gem *)gemWithColor:(NSString *)color;
@end
