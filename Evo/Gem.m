//
//  Gem.m
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Gem.h"


@implementation Gem

@synthesize type;

+ (Gem *)gemWithColor:(NSString *)color 
{
    // creates a gem an gives it a color overlay
    Gem *gem = (Gem *) [[Gem alloc] initWithFile:@"gem.png"];
    
    gem.type = color;
    
    if(color == @"blue")
        [gem setColor:ccc3(100,100,255)];
    else if (color == @"green")
        [gem setColor:ccc3(100,255,100)];
    else if (color == @"red")
        [gem setColor:ccc3(255,100,100)];
    
    return gem;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}
@end
