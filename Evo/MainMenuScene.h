//
//  MainMenuScene.h
//  Evo
//
//  Created by Joseph Ong on 4/24/12.
//  Copyright 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameScene.h"
#import "CCTouchDispatcher.h"
#import "SimpleAudioEngine.h"
#import "CocosDenshion.h"
#import "CDAudioManager.h"

@interface MainMenuScene : CCLayer {
    
}

@property (nonatomic, assign) CCSprite *start;

+(id) scene;

@end
