//
//  Enemy.h
//  Evo
//
//  Created by Joseph Ong on 4/24/12.
//  Copyright 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Joint.h"
#import "Creature.h"
#import "GameModel.h"
#import "Gem.h"

@interface Enemy : CCSprite {
    // declare as protected so subclasses have access
    NSMutableArray *limbs;
    NSMutableArray *joints;
    double currentSpeed;
    double maxSpeed;
    double currentAngle;
    double turnSpeed;
    double HP, maxHP;
    int pulseHitByCount;
    CGPoint randomDestination;
    BOOL overrideDestination;
    int complexity;
}

// array of CCSprites
@property (nonatomic, assign) NSMutableArray *limbs;

// array of CGPoints representing joints
@property (nonatomic, assign) NSMutableArray *joints;

// movement characteristics
@property (nonatomic, assign) double currentSpeed;
@property (nonatomic, assign) double currentAngle;
@property (nonatomic, assign) double maxSpeed;
@property (nonatomic, assign) double turnSpeed;

// creature stats
@property (nonatomic, assign) double HP;
@property (nonatomic, assign) double HPRegen;
@property (nonatomic, assign) double maxHP;
@property (nonatomic, assign) int pulseHitByCount;
@property (nonatomic, assign) BOOL dying;

- (void)colorByHP;
- (void)die;

// for wandering around the map
@property (nonatomic, assign) BOOL overrideDestination;
@property (nonatomic, assign) CGPoint randomDestination;

// number of joints
@property (nonatomic, assign) int complexity;

- (void)conformOverTime:(ccTime) dt;
- (void)updateRandomDestination:(ccTime) dt;
@end

@interface ElectricEel : Enemy {
}

// return an eletric eel
+ (Enemy *)enemy;

@end

@interface Starfish: Enemy {
}

// return a starfish
+ (Enemy *)enemy;

@end

@interface Plankton: Enemy {
}

// return a plankton
+ (Enemy *)enemy;

@end

@interface Tadpole: Enemy {
}

// return a tadpole
+ (Enemy *)enemy;

@end