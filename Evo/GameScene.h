//
//  GameScene.h
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameModel.h"
#import "GameLayer.h"
#import "ControlsLayer.h"
#import "UpgradePanel.h"

@interface GameScene : CCLayer {
    
}

+ (id)scene;

@end
