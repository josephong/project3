//
//  GameModel.m
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameModel.h"
#import "GameLayer.h"
#import "Creature.h"

static GameModel *sharedGameModel = nil;

@implementation GameModel

@synthesize player;
@synthesize level;
@synthesize enemies;
@synthesize gems;
@synthesize gameLayer;
@synthesize mapLayers;
@synthesize upgradePanel;
@synthesize controlsLayer;
@synthesize playing;
@synthesize resources;
@synthesize upgrades;
@synthesize upgradeCosts;
@synthesize upgradeLevels;
@synthesize upgradeTypes;

+ (GameModel *)getModel 
{
    @synchronized(self) 
    {
        if (sharedGameModel == nil)
            sharedGameModel = [[self alloc] init];
    }
    return sharedGameModel;
}


- (id)init {
    if (self = [super init]) 
    {
        enemies = [[NSMutableArray alloc] init];
        mapLayers = [[NSMutableArray alloc] init];
        level = ccp(0, 0);
        gems = [[NSMutableArray alloc] init];
        
        resources = [[NSMutableDictionary alloc] init];
        
        // set up dictionaries of upgrades, costs, and player's upgrade levels
        upgrades = [[NSMutableArray alloc] initWithObjects:@"HPMax", @"pulseRadius", @"movementSpeed", @"HPRegen", @"pulseFrequency", @"gemAttraction", nil];
        upgradeCosts = [[NSMutableDictionary alloc] init];
        upgradeLevels = [[NSMutableDictionary alloc] init];
        
        // this should be taken from plist eventually - quick fix for now, since we're
        // not sure what all our upgrades are eventually going to be
        upgradeTypes = [[NSMutableDictionary alloc] init];
        [upgradeTypes setObject:@"green" forKey:@"HPMax"];
        [upgradeTypes setObject:@"red" forKey:@"pulseRadius"];
        [upgradeTypes setObject:@"blue" forKey:@"movementSpeed"];
        [upgradeTypes setObject:@"green" forKey:@"HPRegen"];
        [upgradeTypes setObject:@"red" forKey:@"pulseFrequency"];
        [upgradeTypes setObject:@"blue" forKey:@"gemAttraction"];
        
        // set the default cost and level to 5 and 0, respectively
        for(NSString* upgrade in upgrades) {
            [upgradeCosts setObject:[NSNumber numberWithInt:5] forKey:upgrade];
            [upgradeLevels setObject:[NSNumber numberWithInt:0] forKey:upgrade];
        }
    }
    return self;
}
@end