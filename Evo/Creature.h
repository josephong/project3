//
//  Creature.h
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Joint.h"
#import "GameModel.h"

@interface Creature : CCSprite {
 
}

// array of CCSprites
@property (nonatomic, assign) NSMutableArray *limbs;

// array of CGPoints representing joints
@property (nonatomic, assign) NSMutableArray *joints;

// movement characteristics
@property (nonatomic, assign) double currentSpeed;
@property (nonatomic, assign) double maxSpeed;
@property (nonatomic, assign) double currentAngle;
@property (nonatomic, assign) double turnSpeed;

// gem attraction factor
@property (nonatomic, assign) double gemAttraction;

// number of joints
@property (nonatomic, assign) int complexity;

// health
@property (nonatomic, assign) double HP;
@property (nonatomic, assign) double maxHP;
@property (nonatomic, assign) double HPRegen;

// attack
@property (nonatomic, assign) double pulseRadius;
@property (nonatomic, assign) double damage;
@property (nonatomic, assign) double pulseSpeed;
@property (nonatomic, assign) double pulseFrequency;
@property (nonatomic, assign) CCSprite *pulse;

// used to track last pulse which hit creature so that pulses only hit once
@property (nonatomic, assign) int pulseCount;

// kill count
@property (nonatomic, assign) int killCount;

+ (Creature *)getCreature;
- (int)isAtEdge;
- (void)conform:(CGPoint)destination overTime:(ccTime) dt;
- (void)pulseOverTime:(ccTime)dt;
- (void)colorByHP;
- (void)heal;
- (void)regen;
- (void)addJoint;
- (double)currentPulseRadius;
@end
