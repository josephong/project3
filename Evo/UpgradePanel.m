//
//  UpgradePanel.m
//  Evo
//
//  Created by John Harvard on 5/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UpgradePanel.h"


@implementation UpgradePanel

@synthesize background;
@synthesize redCountLabel;
@synthesize greenCountLabel;
@synthesize blueCountLabel;
@synthesize pulseRadiusPurchaseBtn;
@synthesize maxHealthPurchaseBtn;
@synthesize movementSpeedPurchaseBtn;
@synthesize pulseFrequencyPurchaseBtn;
@synthesize healthRegenPurchaseBtn;
@synthesize gemAttractionPurchaseBtn;
@synthesize pulseRadiusLabel;
@synthesize maxHealthLabel;
@synthesize movementSpeedLabel;
@synthesize costLabels;
@synthesize upgradeButtons;
@synthesize progressBars;

#define MAX_UPGRADE_LEVEL 10

- (id)init {
    if (self = [super init]) 
    {
        // allocate dictionary for references to labels, buttons, progress bars
        costLabels = [[NSMutableDictionary alloc] init];
        upgradeButtons = [[NSMutableDictionary alloc] init];
        progressBars = [[NSMutableDictionary alloc] init];
        
        // add the pretty evolution panel
        background = [[CCSprite alloc] initWithFile:@"evolution-panel.png"];
        [self addChild:background z:1];
        [background setPosition:ccp(512,384)];
        
        // progress bar for pulse radius upgrade
        CCProgressTimer *pulseRadiusProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        pulseRadiusProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:pulseRadiusProgress z:2];
        [pulseRadiusProgress setScaleX:1.51];
        [pulseRadiusProgress setPosition:ccp(214,514)];
        [pulseRadiusProgress setPercentage:0];
        [progressBars setObject:pulseRadiusProgress forKey:@"pulseRadius"];
        
        // progress bar for max health upgrade
        CCProgressTimer *maxHealthProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        maxHealthProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:maxHealthProgress z:2];
        [maxHealthProgress setScaleX:1.51];
        [maxHealthProgress setPosition:ccp(480,514)];
        [maxHealthProgress setPercentage:0];
        [progressBars setObject:maxHealthProgress forKey:@"HPMax"];
        
        // progress bar for movement speed upgrade
        CCProgressTimer *movementSpeedProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        movementSpeedProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:movementSpeedProgress z:2];
        [movementSpeedProgress setScaleX:1.51];
        [movementSpeedProgress setPosition:ccp(747,514)];
        [movementSpeedProgress setPercentage:0];
        [progressBars setObject:movementSpeedProgress forKey:@"movementSpeed"];
        
        // progress bar for pulse radius upgrade
        CCProgressTimer *pulseFrequencyProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        pulseFrequencyProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:pulseFrequencyProgress z:2];
        [pulseFrequencyProgress setScaleX:1.51];
        [pulseFrequencyProgress setPosition:ccp(215,397)];
        [pulseFrequencyProgress setPercentage:0];
        [progressBars setObject:pulseFrequencyProgress forKey:@"pulseFrequency"];
        
        // progress bar for HP regen
        CCProgressTimer *HPRegenProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        HPRegenProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:HPRegenProgress z:2];
        [HPRegenProgress setScaleX:1.51];
        [HPRegenProgress setPosition:ccp(481,397)];
        [HPRegenProgress setPercentage:0];
        [progressBars setObject:HPRegenProgress forKey:@"HPRegen"];
        
        // progress bar for gem attraction upgrade
        CCProgressTimer *gemAttractionProgress = [[CCProgressTimer alloc] initWithFile:@"progress-bar.png"];
        gemAttractionProgress.type = kCCProgressTimerTypeHorizontalBarLR;
        [self addChild:gemAttractionProgress z:2];
        [gemAttractionProgress setScaleX:1.51];
        [gemAttractionProgress setPosition:ccp(747,396)];
        [gemAttractionProgress setPercentage:0];
        [progressBars setObject:gemAttractionProgress forKey:@"gemAttraction"];
        
        // create sprites for the purchase buttons
        pulseRadiusPurchaseBtn   = [[CCSprite alloc] initWithFile:@"red-purchase.png"];
        maxHealthPurchaseBtn     = [[CCSprite alloc] initWithFile:@"green-purchase.png"];
        movementSpeedPurchaseBtn = [[CCSprite alloc] initWithFile:@"blue-purchase.png"];
        pulseFrequencyPurchaseBtn = [[CCSprite alloc] initWithFile:@"red-purchase.png"];
        healthRegenPurchaseBtn     = [[CCSprite alloc] initWithFile:@"green-purchase.png"];
        gemAttractionPurchaseBtn = [[CCSprite alloc] initWithFile:@"blue-purchase.png"];
        
        // add upgrade buttons to the dictionary
        [upgradeButtons setObject:pulseRadiusPurchaseBtn forKey:@"pulseRadius"];
        [upgradeButtons setObject:maxHealthPurchaseBtn forKey:@"HPMax"];
        [upgradeButtons setObject:movementSpeedPurchaseBtn forKey:@"movementSpeed"];
        [upgradeButtons setObject:pulseFrequencyPurchaseBtn forKey:@"pulseFrequency"];
        [upgradeButtons setObject:healthRegenPurchaseBtn forKey:@"HPRegen"];
        [upgradeButtons setObject:gemAttractionPurchaseBtn forKey:@"gemAttraction"];
        
        // render the purchase buttons
        [self addChild:pulseRadiusPurchaseBtn z:2];
        [self addChild:maxHealthPurchaseBtn z:2];
        [self addChild:movementSpeedPurchaseBtn z:2];
        [self addChild:pulseFrequencyPurchaseBtn z:2];
        [self addChild:healthRegenPurchaseBtn z:2];
        [self addChild:gemAttractionPurchaseBtn z:2];
        
        // set their positions
        [pulseRadiusPurchaseBtn setPosition:ccp(335, 515)];
        [maxHealthPurchaseBtn setPosition:ccp(600, 515)];
        [movementSpeedPurchaseBtn setPosition:ccp(866, 515)];
        [pulseFrequencyPurchaseBtn setPosition:ccp(335, 397)];
        [healthRegenPurchaseBtn setPosition:ccp(600, 397)];
        [gemAttractionPurchaseBtn setPosition:ccp(866, 397)];
        
        // add their respective labels for number of gems possessed in each category
        redCountLabel = [[CCLabelTTF alloc] initWithString:@"0" fontName:@"League Gothic.otf" fontSize:26];
        [self addChild:redCountLabel z:2];
        [redCountLabel setPosition:ccp(246, 130)];
        
        greenCountLabel = [[CCLabelTTF alloc] initWithString:@"0" fontName:@"League Gothic.otf" fontSize:26];
        [self addChild:greenCountLabel z:2];
        [greenCountLabel setPosition:ccp(512,130)];
        
        blueCountLabel = [[CCLabelTTF alloc] initWithString:@"0" fontName:@"League Gothic.otf" fontSize:26];
        [self addChild:blueCountLabel z:2];
        [blueCountLabel setPosition:ccp(779, 130)];
        
        // labels for costs on the various buttons
        CCLabelTTF *pulseRadiusCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [pulseRadiusCostLabel setPosition:ccp(332, 515)];
        [pulseRadiusCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:pulseRadiusCostLabel forKey:@"pulseRadius"];
        
        CCLabelTTF *hpCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [hpCostLabel setPosition:ccp(597,515)];
        [hpCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:hpCostLabel forKey:@"HPMax"];
        
        CCLabelTTF *movementSpeedCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [movementSpeedCostLabel setPosition:ccp(863,515)];
        [movementSpeedCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:movementSpeedCostLabel forKey:@"movementSpeed"];
        
        CCLabelTTF *pulseFrequencyCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [pulseFrequencyCostLabel setPosition:ccp(332, 397)];
        [pulseFrequencyCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:pulseFrequencyCostLabel forKey:@"pulseFrequency"];
        
        CCLabelTTF *healthRegenCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [healthRegenCostLabel setPosition:ccp(597,397)];
        [healthRegenCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:healthRegenCostLabel forKey:@"HPRegen"];
        
        CCLabelTTF *gemAttractionCostLabel = [[CCLabelTTF alloc] initWithString:@"5" fontName:@"League Gothic.otf" fontSize:18];
        [gemAttractionCostLabel setPosition:ccp(863,397)];
        [gemAttractionCostLabel setColor:ccc3(40,40,40)];
        [costLabels setObject:gemAttractionCostLabel forKey:@"gemAttraction"];
        
        // add the cost labels to the layer
        for (id key in costLabels)
            [self addChild:(CCLabelTTF *)[costLabels objectForKey:key] z:2];
        
        self.isTouchEnabled = YES;
    }
    return self;
}

- (void)registerWithTouchDispatcher 
{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (void)toggle 
{
    GameModel *gameModel = [GameModel getModel];
    gameModel.playing = !gameModel.playing;
    
    // update the resource labels based on the user's collected gems
    [self updateResourceLabels];
    
    
    // callback to toggle panel visibility
    id toggleVisibility = [CCCallBlock actionWithBlock:^(void)
    {
        self.visible = !self.visible;
    }];
    
    // fade in, or fade out, depending on the current visibility
    if(self.visible)
    {
        id fade = [CCFadeOut actionWithDuration:.3];
        [self runAction:[CCSequence actions:fade, toggleVisibility, nil]];
    }
    else {
        id fade = [CCFadeIn actionWithDuration:.3];
        [self runAction:[CCSequence actions:toggleVisibility, fade, nil]];
        
    }
}


- (void)updateResourceLabels 
{
    // changes the labels to reflect gems collected by user
    GameModel *gameModel = [GameModel getModel];
    [redCountLabel setString:[(NSNumber *)[[gameModel resources] objectForKey:@"red"] stringValue]];
    [greenCountLabel setString:[(NSNumber *)[[gameModel resources] objectForKey:@"green"] stringValue]];
    [blueCountLabel setString:[(NSNumber *)[[gameModel resources] objectForKey:@"blue"] stringValue]];

}

- (void)updateProgressBars 
{
    GameModel *gameModel = [GameModel getModel];
    
    for (id key in progressBars) 
    {
        CCProgressTimer *progressBar = (CCProgressTimer *) [progressBars objectForKey:key];
        
        // get upgrade level from 0-10, stretches progress bar
        int upgradeLevel = [(NSNumber *) [[gameModel upgradeLevels] objectForKey:key] intValue];
        [progressBar setPercentage:(10*upgradeLevel)];
    }
}

- (void)updateButtons 
{
    GameModel *gameModel = [GameModel getModel];
    
    // updates the buttons based on the current upgrade level
    for (id key in upgradeButtons) 
    {
        int upgradeLevel = [(NSNumber *) [[gameModel upgradeLevels] objectForKey:key] intValue];
        CCLabelTTF *costLabel = (CCLabelTTF *) [costLabels objectForKey:key];
        [costLabel setString:[NSString stringWithFormat:@"%@", [[gameModel upgradeCosts] objectForKey:key]]];
        
        // maxes out at level 10
        if (upgradeLevel == MAX_UPGRADE_LEVEL)          
            [costLabel setString:@"MAX"];
    }
}



- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event 
{
    GameModel *gameModel = [GameModel getModel];
    
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    // don't register touch if this panel is invisible
    if (!self.visible) return NO;
    
    // if we touch within the bounding box, keep checking, else return to game
    if (CGRectContainsPoint([background boundingBox], touchLocation)) 
    {
        // for key in our array
        for(id key in upgradeButtons) 
        {
            // grab the button associated with it
            CCSprite *button = [upgradeButtons objectForKey:key];
            
            // check if we're touching that button
            if(CGRectContainsPoint([button boundingBox], touchLocation)) 
            {
                // check for the cost of the upgrade
                int cost = [[[gameModel upgradeCosts] objectForKey:key] intValue];
                NSString *upgradeType = (NSString *) [[gameModel upgradeTypes] objectForKey:key];
                
                // the amount of player gems of that color
                int resourceOfType = [(NSNumber *) [[gameModel resources] objectForKey:upgradeType] intValue];
                
                // if the player has enough to purchase the upgrade
                if(resourceOfType >= cost) 
                {
                    // purchase it
                    int upgradeLevel = [(NSNumber *) [[gameModel upgradeLevels] objectForKey:key] intValue];
                    
                    // don't exceed level 10, our max level
                    if (upgradeLevel >= MAX_UPGRADE_LEVEL) break;
                    
                    // update the model with the upgrade level, new cost, and remaining gems for player
                    [[gameModel upgradeLevels] setObject:[NSNumber numberWithInt:(upgradeLevel + 1)] forKey:key];
                    [[gameModel upgradeCosts] setObject:[NSNumber numberWithInt:(cost + 5)] forKey:key];
                    [[gameModel resources] setObject:[NSNumber numberWithInt:(resourceOfType - cost)] forKey:upgradeType];
                    
                    // update the gui elements
                    [self updateProgressBars];
                    [self updateButtons];
                    [self updateResourceLabels];
                    
                    // heal if an HP purchase upgrade was bought
                    if([key isEqualToString:@"HPMax"])
                        [[gameModel player] heal];
                }
                
                // don't check anymore buttons
                break;
            }
        }
    } 
    else 
    {
        // return to game if we touched outside the box
        [self toggle];
    }
    
    return YES;
}
@end
