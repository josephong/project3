//
//  Creature.m
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Creature.h"


@implementation Creature

@synthesize limbs;
@synthesize joints;
@synthesize complexity;
@synthesize currentSpeed;
@synthesize currentAngle;
@synthesize turnSpeed;
@synthesize HP;
@synthesize maxHP;
@synthesize HPRegen;
@synthesize pulseRadius;
@synthesize pulseSpeed;
@synthesize pulseFrequency;
@synthesize damage;
@synthesize pulse;
@synthesize pulseCount;
@synthesize maxSpeed;
@synthesize gemAttraction;
@synthesize killCount;

#define LEFT -1
#define RIGHT 1
#define BOTTOM -2
#define TOP 2
#define CENTER 0
#define EDGE_THRESHOLD 30
#define HP_UPGRADE_FACTOR 10
#define PULSE_DIAMETER_UPGRADE_FACTOR 10
#define PULSE_FREQUENCY_UPGRADE_FACTOR 20.0f
#define SPEED_UPGRADE_FACTOR 15
#define DEFAULT_CREATURE_LENGTH 4
#define NUM_JOINT_SPRITES 3
#define JOINT_PIXEL_OFFSET 30
#define HEALTH_COLOR_OFFSET 55
#define PULSE_SPRITE_DIAMETER 432.0
#define DECELERATION_DISTANCE 200
#define DECELERATION_FACTOR 6
#define ACCELERATION_FACTOR 10

/*
 * Returns a creature playable by the character
 *
 */
+ (Creature *)getCreature 
{
    Creature *creature = [Creature node];
    
    // a creature is made up of four joints
    for(int i = 0; i < DEFAULT_CREATURE_LENGTH; i++) 
    {
        // set the endpoints so they end up in a trail
        Joint *joint = (Joint*) [Joint jointWithFile:[NSString stringWithFormat:@"Joint%d-small.png", (i % NUM_JOINT_SPRITES)+1]];
        [joint setEndpoints:ccp(0, i * JOINT_PIXEL_OFFSET) :ccp(0, JOINT_PIXEL_OFFSET + i * JOINT_PIXEL_OFFSET)];
        CGPoint jointPosition = ccp(0, i * JOINT_PIXEL_OFFSET);
        
        // add to an array keeping track of the joints
        [[creature joints] addObject:NSStringFromCGPoint(jointPosition)];
        [[creature limbs] addObject:joint];
        [creature addChild:joint z:3];
        
        // increment complexity of creature (i.e. number of joints)
        creature.complexity++;
    }
    
    // base stats for our creature
    creature.currentSpeed = 170;
    creature.maxSpeed = 300;
    creature.HP = 50;
    creature.maxHP = 50;
    creature.HPRegen = 2;
    creature.pulseRadius = 50;
    creature.pulseCount = 0;
    creature.pulseSpeed = 0.4;
    creature.damage = 15;
    creature.gemAttraction = 0;
    
    // set up sprite for our attack weapon
    creature.pulse = [[CCSprite alloc] initWithFile:@"pulse-circle.png"];
    [creature.pulse setScale:0.0];
    [creature.pulse setOpacity:80];
    [creature addChild:creature.pulse z:2];
    
    [creature colorByHP];
    
    return creature;
}


- (id)init 
{
    if (self = [super init]) 
    {
        joints = [[NSMutableArray alloc] init];
        limbs = [[NSMutableArray alloc] init];
        complexity = 0;
        turnSpeed = 10;
        killCount = 0;
    }
    return self;
}

// adds a joint to the player
- (void) addJoint 
{
    Joint *joint = (Joint*) [Joint jointWithFile:[NSString stringWithFormat:@"Joint%d-small.png", (complexity % NUM_JOINT_SPRITES)+1]];
    [joint setEndpoints:ccp(0, complexity * JOINT_PIXEL_OFFSET) :ccp(0, JOINT_PIXEL_OFFSET + complexity * JOINT_PIXEL_OFFSET)];
    CGPoint jointPosition = ccp(0, complexity * JOINT_PIXEL_OFFSET);
    
    // add to an array keeping track of the joints
    [[self joints] addObject:NSStringFromCGPoint(jointPosition)];
    [[self limbs] addObject:joint];
    [self addChild:joint z:3];
    
    id fadeIn = [CCFadeIn actionWithDuration:1];
    [joint runAction:fadeIn];
    
    self.complexity++;
}

- (void)colorByHP 
{
    // determine the upgraded max hp based on upgrade purchases
    GameModel *gameModel = [GameModel getModel];
    double upgradedMaxHP = maxHP + HP_UPGRADE_FACTOR * [(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"HPMax"] intValue];
    
    // color the player a shade of red depending on the differences
    int difference = HEALTH_COLOR_OFFSET * HP/upgradedMaxHP;
    for(Joint *limb in [self limbs])
        [limb setColor: ccc3(255 - difference, 200 + difference, 200)];
}

- (void)heal 
{
    // completely heals the player
    GameModel *gameModel = [GameModel getModel];
    int upgradedMaxHP = maxHP + HP_UPGRADE_FACTOR * [(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"HPMax"] intValue];
    [self setHP:upgradedMaxHP];
    [self colorByHP];
}

- (void)regen 
{
    GameModel *gameModel = [GameModel getModel];
    
    // calculate the new HP after regeneration is taken into account
    int newHP = HP + HPRegen + [(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"HPRegen"] intValue];
    int upgradedMaxHP = maxHP + HP_UPGRADE_FACTOR * [(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"HPMax"] intValue];
    
    // avoids exceeding the max HP
    if(newHP > upgradedMaxHP)
        newHP = upgradedMaxHP;
    
    [self setHP:newHP];
    [self colorByHP];
}

// function that checks whether player is at the edge of the map
- (int) isAtEdge 
{
    CCSprite *mapLayer = [[[GameModel getModel] mapLayers] objectAtIndex:0];
    
    // find the bounding boxes of the map position
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGFloat left   = -winSize.width/2;
    CGFloat right  = mapLayer.boundingBox.size.width + left;
    CGFloat bottom = -winSize.height/2;
    CGFloat top    = mapLayer.boundingBox.size.height + bottom;
    
    // considered "at an edge" if less than some distance from the edge
    if(self.position.x - EDGE_THRESHOLD < left)
        return LEFT;
    
    if(self.position.x + EDGE_THRESHOLD > right)
        return RIGHT;
    
    if(self.position.y - EDGE_THRESHOLD < bottom)
        return BOTTOM;
    
    if(self.position.y + EDGE_THRESHOLD > top)
        return TOP;
    
    return CENTER;
}

- (void)pulseOverTime:(ccTime)dt 
{
    GameModel *gameModel = [GameModel getModel];
    
    // compute current radius, max upgrade radius, and pulse frequency
    double currentRadius = [self.pulse scale] * PULSE_SPRITE_DIAMETER / 2;
    double maxUpgradedPulseRadius = pulseRadius + PULSE_DIAMETER_UPGRADE_FACTOR * [(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"pulseRadius"] intValue];
    double upgradedPulseFrequency = ([(NSNumber *) [[gameModel upgradeLevels] objectForKey:@"pulseFrequency"] intValue] + 1)/PULSE_FREQUENCY_UPGRADE_FACTOR + 1;
    
    // calculate new scale for pulse sprite based on those values
    double newScale = [self.pulse scale] + dt * .01 * upgradedPulseFrequency * maxUpgradedPulseRadius * pulseSpeed;
    [self.pulse setScale:newScale];
    
    // if the current radius of the scale exceeds our max, start the pulse over
    if(currentRadius > maxUpgradedPulseRadius) {
        [self.pulse setScale:0];
        self.pulseCount++;
    }
    
    // reposition the pulse on the first limb
    [self.pulse setPosition:[[[self limbs] objectAtIndex:0] position]];
    
}

- (void)conform:(CGPoint)destination overTime:(ccTime)dt
{
    GameModel *gameModel = [GameModel getModel];
    
    // calculate the max speed based on upgrades
    double maxUpgradedSpeed = maxSpeed + [(NSNumber *)[[gameModel upgradeLevels] objectForKey:@"movementSpeed"] intValue] * SPEED_UPGRADE_FACTOR;
    
    // if you are approaching your destination, decelerate to a stop
    // else, you are far away from your destination, so accelerate up to the max speed
    if(ccpDistance(destination, self.position) < DECELERATION_DISTANCE)
        currentSpeed = (currentSpeed < DECELERATION_FACTOR) ? 0 : currentSpeed - DECELERATION_FACTOR;
    else
        currentSpeed = (currentSpeed > maxUpgradedSpeed) ? maxUpgradedSpeed : currentSpeed + ACCELERATION_FACTOR;
    
    // get the angle to the target's destination
    CGPoint diffInPosition = ccpSub(destination, self.position);
    double destAngle = atan2(diffInPosition.y, diffInPosition.x);
    
    // make sure the angle is bounded between 0 and 2pi
    if(destAngle < 0) destAngle += 2 * M_PI;
    if(destAngle > 2 * M_PI) destAngle -= 2 * M_PI;
    
    // make sure our current angle is bounded between 0 and 2pi
    if(currentAngle > 2 * M_PI) currentAngle -= 2 * M_PI;
    if(currentAngle < 0) currentAngle += 2 * M_PI;
    
    // compare the angles
    if(currentAngle < destAngle) 
    {
        // if within a certain threshold, just rotate completely
        if((destAngle - currentAngle) <= turnSpeed * dt) 
        {
            currentAngle = destAngle;
        }
        else 
        {
            // else, turn slowly to face that direction
            if(destAngle - currentAngle >= M_PI)
                currentAngle -= turnSpeed * dt;
            else
                currentAngle += turnSpeed * dt;
            
        } 
    }
    
    // if the opposite holds true, rotate the other direction
    if(currentAngle >= destAngle) 
    {
        if((-destAngle + currentAngle) <= turnSpeed * dt)
            currentAngle = destAngle;
        else 
        {
            if(currentAngle - destAngle >= M_PI)
                currentAngle += turnSpeed * dt;
            else
                currentAngle -= turnSpeed * dt;
        }
        
    }
    
    // make sure the angle is still bounded between 0 and 2pi after alteration
    if(currentAngle > 2 * M_PI) currentAngle -= 2 * M_PI;
    if(currentAngle < 0) currentAngle += 2 * M_PI;
    
    // update the rotation of the head
    [[self.limbs objectAtIndex:0] updateAngle:currentAngle];
    
    // calculate the delta distance from the current point
    double diffX = currentSpeed * cos(currentAngle) * dt;
    double diffY = currentSpeed * sin(currentAngle) * dt;
    CGPoint constrainedDiffInPosition = ccp(diffX, diffY);
    
    // change our position by that delta
    self.position = ccpAdd(self.position, constrainedDiffInPosition);
    
    // shift all subsequent joints so they rotate and move in the direction of the prior joint
    for (int i = 1; i < complexity; i++) 
    {
        Joint *joint = [self.limbs objectAtIndex:i];
        Joint *previousJoint = [self.limbs objectAtIndex:i-1];
        
        [joint updateAngle:ccpToAngle(ccpSub([previousJoint position], [joint secondEndpoint]))];
        joint.position = ccpAdd([previousJoint secondEndpoint], ccpSub(joint.position, [joint firstEndpoint]));
    }
    [self colorByHP];
}

- (double)currentPulseRadius 
{
    return [self.pulse scale] * PULSE_SPRITE_DIAMETER / 2;
}

@end
