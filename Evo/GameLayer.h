//
//  GameLayer.h
//  Evo
//
//  Created by El Señor Martín del Camacho on 4/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Creature.h"
#import "Enemy.h"
#import "GameModel.h"
#import "CCTouchDispatcher.h"
#import "Gem.h"
#import "SimpleAudioEngine.h"

@interface GameLayer : CCLayer {
    
}

@property (nonatomic, assign) CGPoint movementTouchLocation;
@property (nonatomic, assign) CGPoint movementTouchLocationGL;
@property (nonatomic, assign) BOOL movementTouched;
@property (nonatomic, assign) CCSprite *map;
@property (nonatomic, assign) CCLabelTTF *levelLabel;
@property (nonatomic, assign) CCLabelTTF *levelInstructions;
@property (nonatomic, assign) CCLabelTTF *killInstructions;

- (void)creatureLogic:(ccTime)dt;
- (void)gemLogic:(ccTime)dt;
- (void)damageLogic:(ccTime)dt;
- (void)updateTouchLocations;
- (void)generateRandomEnemies;
- (void)destroyAllEnemies;

@end
